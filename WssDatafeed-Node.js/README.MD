# Tích hợp server nodejs của khách hàng với websosanh

## nếu server khách hàng host trên express

1. Tìm đến thư mục host application của khách hàng, thông thường trong thư mục đó sẽ có thư mục "node_modules"
   Mở console và gõ lệnh:

```sh
npm i http-proxy-middleware -S
```

tìm file application của khách hàng, thông thường là file có lệnh `require("express")` và có biến được gán bằng express()
vd: var app=express(); 2. thêm vào cuối application đoạn sau:

```js
const { createProxyMiddleware } = require("http-proxy-middleware");
app.use(
  "/websosanh",
  createProxyMiddleware({ target: "http://localhost:3001", changeOrigin: true })
);
```

# Chạy sample project:

vào thư mục WssDatafeed-nodejs
chạy lệnh sau từ của sổ console

```sh
npm i
node server.js & node test-server-proxy.js
```

test request

```sh
curl http://localhost:3000/websosanh/?page=1
```
