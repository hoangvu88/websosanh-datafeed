/**
 * Đây là file server của khách hàng
 */

var express=require("express");
var app=express();
const { createProxyMiddleware } = require('http-proxy-middleware');

/**
 * Chuyển hướng các request bắt đầu với /websosanh được phục vụ bởi server local chạy trên port 3001
 */
app.use('/websosanh', createProxyMiddleware({ target: 'http://localhost:3001', changeOrigin: true }));

app.listen(3000,function(){
    console.log("server listen on port 3000")
});